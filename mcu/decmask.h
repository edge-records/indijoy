#ifndef _DECMASK_
#define _DECMASK_
#include <avr/io.h>
#include "confmenu.h"

// Decision Mask struct
typedef struct {
  uint8_t f1;
  uint8_t f2;
  uint8_t f3;
  uint8_t up;
  uint8_t cfg;
} decmask;

void setdecmask(preset *thispreset, decmask *thismask);

#endif /* _DECMASK_ */

