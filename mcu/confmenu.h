#ifndef _CONFMENU_
#define _CONFMENU_
typedef struct {
  uint8_t mode;
  uint8_t buttonA;
  uint8_t buttonB;
  uint8_t buttonC;
  uint8_t up;
  uint8_t hifreq;
  uint8_t lofreq;
  uint8_t rfreach;
  uint8_t disp;
  uint8_t brightness;
  uint8_t greeting;
} preset;

void runmenu(uint8_t *presetindex, preset *thispreset);

preset readpreset(uint8_t);

void updatepreset(uint8_t, preset);

void initpreset(uint8_t);

uint8_t readpresetindex(void);

//EEMEM extern preset prsdefault;
#endif /* _CONFMENU_ */
