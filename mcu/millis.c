#include <stdint.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "millis.h"

volatile unsigned long timer1_millis;

ISR (TIMER1_COMPB_vect)
{
    timer1_millis++;
}

unsigned long millis ()
{
    unsigned long millis_return;

    // Ensure this cannot be disrupted
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        millis_return = timer1_millis;
    }

    return millis_return;
}

