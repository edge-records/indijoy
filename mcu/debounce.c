/**
 * debounce.c
 **/

#include "debounce.h"

// Bits is set to one if a depounced press is detected.
//uint8_t currentjoy;

// Eight vertical two bit counters for number of equal states
static uint8_t vcount_low = 0xFF, vcount_high = 0xFF;
// Keeps track of current (debounced) state
static uint8_t button_state = 0;

// Check button state and set bits in the currentjoy variable if a
// debounced button down press is detected.
// Call this function every 10 ms or so.
void debounce(uint8_t negcjoy, uint8_t *button_down) {
  // Read buttons (active low so invert with ~). Xor with
  // button_state to see which ones are about to change state
  uint8_t state_changed = negcjoy ^ button_state;

  // Decrease counters where state_changed = 1, set the others to 0b11.
  VC_DEC_OR_SET(vcount_high, vcount_low, state_changed);

  // Update state_changed to have a 1 only if the counter overflowed
  state_changed &= vcount_low & vcount_high;
  // Change button_state for the buttons who’s counters rolled over
  button_state ^= state_changed;

  // Update currentjoy with buttons who’s counters rolled over
  // and who’s state is 1 (pressed)
  *button_down |= button_state & state_changed;
}
