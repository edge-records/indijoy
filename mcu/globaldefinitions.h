#ifndef GLOBALDEFINITIONS_H

#define CTC_MATCH_OVERFLOW ((F_CPU / 1000) / 8)

#define JOY ((PINB & 0x01) | ((PIND & 0x80) >> 6) | ((PIND & 0x40) >> 4) | ((PIND & 0x20) >> 2) | ((PINB & 0x1e) << 3))

// These bits are defined based on JOY position
#define LEFT_IN  0 // PB0
#define RIGHT_IN 1 // PD7 right shifted by 6
#define DOWN_IN  2 // PD6 right shifted by 4
#define UP_IN    3 // PD5 right shifted by 2
#define BUTTONA  4 // PB1 left shifted by 3
#define BUTTONB  5 // PB2 left shifted by 3
#define BUTTONC  6 // PB3 left shifted by 3
#define RFIRE    7 // PB4 left shifted by 3

// These bits are defined based on their physical port
#define LEFT_OUT  5 // PC5
#define RIGHT_OUT 4 // PC4
#define DOWN_OUT  0 // PC0
#define UP_OUT    0 // PD0
#define FIRE1     2 // PC2
#define FIRE2     1 // PC1
#define FIRE3     3 // PC3

#endif
