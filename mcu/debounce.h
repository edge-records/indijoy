/**
 * debounce.h. Snigelens version of Peter Dannegger’s debounce routines.
 * Debounce up to eight buttons on one port. $Rev: 577 $
 **/
#ifndef DEBOUNCE_H
#define DEBOUNCE_H

#include <avr/io.h>

// Check button state and set bits in the button_down variable if a
// debounced button down press is detected.
// Call this function every 10 ms or so.
void debounce(uint8_t negcjoy, uint8_t *button_down);

// Decrease 2 bit vertical counter where mask = 1.
// Set counters to binary 11 where mask = 0.
#define VC_DEC_OR_SET(high, low, mask) \
  low = ~(low & mask);                 \
high = low ^ (high & mask)

#endif
