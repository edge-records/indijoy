![IndiJoy](manual/imgs/logo.png "Logo")

# IndiJoy - Individual Joysticks

IndiJoy is a circuit board for DIY joysticks that can be simultaneously connected via USB to PCs and MACs, and via 9 pin Sub-D Atari Game Port to Commodore 64 or Amiga machines, and compatibles. It can either be used to upgrade existing joysticks or gamepads, or to build custom new ones.

![PCB Front](manual/imgs/indijoy-front.jpg "PCB Front")
![PCB Back](manual/imgs/indijoy-back.jpg "PCB Back")


## Features:

* USB connectivity, supported by all major operating systems
* Connectivity via 9 pin Sub-D connector to historic devices like C64 and Amiga, and any compatibles.
* 3 distinct fire buttons – buttons 2 and 3 supported by both C64 and Amiga
* Switch for enabling rapid fire – configurable to which of the fire signals it gets applied
* Rotary knob for setting frequency of rapid fire
* Lowest and highest rapid fire frequency is configurable
* Rapid Fire yields to manual press of corresponding button
* Swap “UP” direction against one of the 3 fire buttons
* 4 x 7 Segment LED display:
    * Display manual or rapid fire frequency
    * Configuration
* 9 savable preset slots in configuration menu

This repo contains the manuals, and the [source code for the Microcontroller firmware](mcu/).

[Click here for the full Instruction Manual PDF](manual/IndiJoy Manual EN.pdf)

[Hier klicken fuer die komplette Bedienungsanleitung](manual/IndiJoy Manual DE.pdf)

## Construction Suggestions:

![Construction suggestion](manual/imgs/CompetitionPro-lookalike.jpg "Construction suggestion")
![Construction suggestion](manual/imgs/Quickshot-lookalike.jpg "Construction suggestion")
