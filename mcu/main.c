/*
 * Indijoy
 *	
 * based on project hid-mouse, a very simple HID example by Christian Starkjohann, OBJECTIVE DEVELOPMENT Software GmbH
 * and ATtiny2313 USB Joyadapter firmware by Grigori Goronzy.	
 * adapted for ATMega328pb
 * 
 * Name: main.c
 * Project: Indijoy
 * Author: Stephan Eckweiler (Andreas Paul, Christian Starkjohann, Grigori Goronzy) 
 * Creation Date: 2008-04-07
 * Tabsize: 4
 * 
 * License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
 * This Revision: $Id: $
 */

/*
# ATmega328PB Pin Layout

| Pin | No | Direction | A/D | Function         |
| --- | -- | --------- | --- | ---------------- |
| PB0 | 12 | In        | D   | Left             |
| PB1 | 13 | In        | D   | ButtonA          |
| PB2 | 14 | In        | D   | ButtonB          |
| PB3 | 15 | In        | D   | ButtonC          |
| PB4 | 16 | In        | D   | RapidFire Toggle |
| PB5 | 17 |           |     | SPI SCK          |
| PB6 |  7 |           | D   | Chrystal 1       |
| PB7 |  8 |           | D   | Chrystal 2       |
| PC0 | 23 | Out       | D   | Down             |
| PC1 | 24 | Out       | D   | Fire2            |
| PC2 | 25 | Out       | D   | Fire1            |
| PC3 | 26 | Out       | D   | Fire3            |
| PC4 | 27 | Out       | D   | Right            |
| PC5 | 28 | Out       | D   | Left             |
| PC6 | 29 |           |     | SPI Reset        |
| PD0 | 30 | Out       | D   | Up               |
| PD1 | 31 | Out       | D   | USB D-           |
| PD2 | 32 | Out       | D   | USB D+           |
| PD3 |  1 | Out       | D   | LCD DIO          |
| PD4 |  2 | Out       | D   | LCD CLK          |
| PD5 |  9 | In        | D   | Up               |
| PD6 | 10 | In        | D   | Down             |
| PD7 | 11 | In        | D   | Right            |
| PE2 | 19 | In        | A   | RapidFire Freq   |
*/

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv.h"
#include "osccal.h"
#include "usb.h"

#include "firefreq.h"
#include <avr/eeprom.h>
#include "tm1637.h"
#include "millis.h"

#include "confmenu.h"
#include "decmask.h"
#include "globaldefinitions.h"

EEMEM uint8_t CALSTORE;

uint8_t presetindex;

PROGMEM const uint8_t Indijoy_segments[14] = {
  0x00, // empty
  0x00, // empty
  0x00, // empty
  0x06, // I
  0x54, // n
  0x5E, // d
  0x04, // i
  0x1F, // J
  0x5C, // o
  0x6E, // y
  0x00, // empty
  0x00, // empty
  0x00, // empty
  0x00, // empty
};

/* ------------------------------------------------------------------------- */

//int __attribute__((noreturn)) main(void) {
int main(void) {
  uint8_t calforsec = eeprom_read_byte(&CALSTORE);
  volatile uint8_t bootBannOffs = 0; // Boot Banner Offset
  uint8_t rfire, lastrfire;
  uint8_t runmenuflag = 0;
  uint32_t milliseconds_current, rfcurrms;
  uint32_t milliseconds_since, rflastms, rflastfire = 0;
  uint32_t rfread, rffreq, rfms;

  presetindex = readpresetindex();
  preset thispreset = readpreset(presetindex);
  decmask thismask = {0};

  // Initialize unused presets
  if (thispreset.buttonA == 0) {
    initpreset(presetindex);
  }

  // Setup Ports
  // PB 6:7 Chrystal
  // PB 5 SPI SCK
  // PB 0:4 digital inputs with Pull-up
  DDRB =  0b00000000;
  PORTB = 0b00011111;

  // PC6:7 not present, so input with pull-up
  // PC4:5 Directional Outputs, default set to HiZ input
  // PC3 Fire output, default set to output low
  // PC2 Fire output, default set to HiZ input
  // PC1 Fire output, default set to output low
  // PC0 Directional Output, default set to HiZ input
  DDRC =  0b00001010;
  PORTC = 0b11000000;

  // PD5:7 digital inputs with Pull-up
  // PD3:4 LCD Output, default low
  // PD1:2 USB input, no pull-up
  // PD0   Directional Output, default set to HiZ input
  DDRD =  0b00011000;
  PORTD = 0b11100000;

  // PE3   not present, so input with pull-up
  // PE2   Rapid Fire Freq, so input, no pull-up
  // PE0:1 not present, so input with pull-up
  DDRE =  0b00000000;
  PORTE = 0b00001011;

  // Enable A/D, set prescaler division to 64
  ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1);

  // Chooses PE2(ADC6) as Analog source, and AVcc as Voltage reference
  ADMUX = 0b01000110;

  // Setup 4x7 Segment display
  TM1637_init(1/*enable*/, (thispreset.brightness/*brightness*/));

  // Setup Milliseconds counter
  // CTC mode, Clock/8
  TCCR1B |= (1 << WGM12) | (1 << CS11);

  // Load the high byte, then the low byte
  // into the output compare
  OCR1AH = (CTC_MATCH_OVERFLOW >> 8);
  OCR1AL = CTC_MATCH_OVERFLOW;

  // Enable the compare match interrupt
  TIMSK1 |= (1 << OCIE1B);

  TM1637_clear();

  uchar   i;
  uchar currentjoy, negcjoy;
  uchar lastjoy = 0x03; 

  /* If Fire2 is pressed while plugging in Joystick, open Configuration menu
   */
  if (~JOY & (1 << BUTTONB)) {
    runmenu(&presetindex, &thispreset);
  }
  setdecmask(&thispreset, &thismask);

  wdt_enable(WDTO_1S);
  /* Even if you don't use the watchdog, turn it off here. On newer devices,
   * the status of the watchdog (on/off, period) is PRESERVED OVER RESET!
   */
  /* RESET status: all port bits are inputs without pull-up.
   * That's the way we need D+ and D-. Therefore we don't need any
   r additional hardware initialization.
   */

  usbInit();
  usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
  i = 0;
  while(--i){             /* fake USB disconnect for > 250 ms */
    wdt_reset();
    _delay_ms(1);
  }
  usbDeviceConnect();

  if (calforsec != 0xff) {
    OSCCAL = calforsec;
  }
  sei();

  for(;;){                /* main event loop */
    wdt_reset();
    usbPoll();

    currentjoy = JOY;
    negcjoy = ~currentjoy;

    if (runmenuflag) {
      runmenuflag = 0;
      TM1637_init(1, thispreset.brightness);
      runmenu(&presetindex, &thispreset);
      setdecmask(&thispreset, &thismask);
      _delay_ms(500);
      continue;
    }

    if ((lastjoy != currentjoy) && ((~currentjoy & thismask.cfg)) != 0) {
      runmenuflag = 1;
      _delay_ms(200);
    }

    if ((negcjoy & (1 << RFIRE)) && ! runmenuflag)  {
      rfcurrms = millis();
      if ((rfcurrms - rflastms) > 50) {
        ADCSRA |= (1 << ADSC);
        while (ADCSRA & (1 << ADSC));
        rfread = ADCW;
        rffreq = ((uint32_t)thispreset.hifreq - (uint32_t)thispreset.lofreq) * 10000 / 1020 * (uint32_t)rfread / 100 + (thispreset.lofreq * 100);
        rffreq = Round(rffreq);
        // 10000 / 2 because 10000 to get rid of zeros to be able to use int, and /2 because we want to toggle twice per cycle, first half on, 2nd half off
        rfms = 5000 / rffreq;
        rflastms = rfcurrms;
      }
      if ((rfcurrms - rflastfire) >= rfms) {
        rfire ^= 1;
        rflastfire = rfcurrms;
      }

    } else {
      rfire = 0;
    }
  
    if (negcjoy & (1 << LEFT_IN)) {
      DDRC |= (1 << LEFT_OUT);
    } else {
      DDRC &= ~(1 << LEFT_OUT);
    }

    if (negcjoy & (1 << RIGHT_IN)) {
      DDRC |= (1 << RIGHT_OUT);
    } else {
      DDRC &= ~(1 << RIGHT_OUT);
    }

    if (negcjoy & (1 << DOWN_IN)) {
      DDRC |= (1 << DOWN_OUT);
    } else {
      DDRC &= ~(1 << DOWN_OUT);
    }

    if (negcjoy & thismask.up) {
      DDRD |= (1 << UP_OUT);
    } else {
      DDRD &= ~(1 << UP_OUT);
    }

    if ((negcjoy & thismask.f1) || ((rfire & 1) && (thispreset.rfreach & 1))) {
      DDRC |= (1 << FIRE1);
    } else {
      DDRC &= ~(1 << FIRE1);
    }

    /* Send button B and C state, either C64 or Amiga style */
    // if C64
    if (thispreset.mode == 0) {
      if ((negcjoy & thismask.f2) || ((rfire & 1) && (thispreset.rfreach & 2))) {
        PORTC |= (1 << FIRE2);
      } else {
        PORTC &= ~(1 << FIRE2);
      }
      if ((negcjoy & thismask.f3) || ((rfire & 1) && (thispreset.rfreach & 4))) {
        PORTC |= (1 << FIRE3);
      } else {
        PORTC &= ~(1 << FIRE3);
      }
    // if Amiga
    } else if (thispreset.mode == 1) {
      if ((negcjoy & thismask.f2) || ((rfire & 1) && (thispreset.rfreach & 2))) {
        PORTC &= ~(1 << FIRE2);
      } else {
        PORTC |= (1 << FIRE2);
      }
      if ((negcjoy & thismask.f3) || ((rfire & 1) && (thispreset.rfreach & 4))) {
        PORTC &= ~(1 << FIRE3);
      } else {
        PORTC |= (1 << FIRE3);
      }
    }

    // if display is on
    if (thispreset.disp == 1) {
      countFire1(currentjoy, lastjoy, &thismask);

      milliseconds_current = millis();
      // if banner isn't done yet and enabled
      if ( bootBannOffs < 11 && (thispreset.greeting == 1)) {
      if (milliseconds_current - milliseconds_since > 350) {
        TM1637_display_segments(0, pgm_read_byte(&Indijoy_segments[bootBannOffs]));
        TM1637_display_segments(1, pgm_read_byte(&Indijoy_segments[bootBannOffs + 1]));
        TM1637_display_segments(2, pgm_read_byte(&Indijoy_segments[bootBannOffs + 2]));
        TM1637_display_segments(3, pgm_read_byte(&Indijoy_segments[bootBannOffs + 3]));
        bootBannOffs++;
        milliseconds_since = milliseconds_current;
      }
      } else if (milliseconds_current - milliseconds_since > 500) {
        if (negcjoy & (1 << RFIRE)) {
          HertzPrint(rffreq);
        } else {
          HertzCalc(milliseconds_current);
        }
        milliseconds_since = milliseconds_current;
      }
    } else {
      // Disable display
      TM1637_init(0,0);
    }

    if (calforsec != OSCCAL) {
      eeprom_write_byte(&CALSTORE, OSCCAL);
      calforsec = eeprom_read_byte(&CALSTORE);
    }

    if(usbInterruptIsReady()){
      /* called after every poll of the interrupt endpoint */

      if ((lastjoy != currentjoy) || (lastrfire != rfire)) {
        lastjoy = currentjoy;
        lastrfire = rfire;
        buildReport(negcjoy, rfire, &thismask, &thispreset);
        usbSetInterrupt((void *)&reportBuffer, sizeof(reportBuffer));

      }

    }

  }
}
/* ------------------------------------------------------------------------- */
