#ifndef USB_H
#define USB_H
#include "confmenu.h"
#include "decmask.h"

typedef struct{
  uint8_t   dx;
  uint8_t   dy;
  uint8_t   buttonMask;
}report_t;

extern report_t reportBuffer;

usbMsgLen_t usbFunctionSetup(uint8_t data[8]);

void buildReport(uint8_t njoy, uint8_t rfire, decmask *thismask, preset *thispreset);

#endif /* USB_H */
