#ifndef FIREFREQ_H
#define FIREFREQ_H

#include "decmask.h"

/**
 * Count all raising flanks of Fire1
 * Also try to debounce the button as good as possible
 */
void countFire1(uint8_t currentjoy, uint8_t formerjoy, decmask *mask);

/**
 * Calculate current frequency, based on
 *   - times Fire1 was counted with countFire1,
 *   - between the current millisecond and one second ago,
 *   - corrected against a full second.
 * Will round against 2 decimal places, to have 1 decimal place accuracy
 * Will call HertzPrint, to update display
 */
void HertzCalc(uint32_t currentms);

/**
 * Round an int sacrificing its least significant decimal place
 */
uint32_t Round(uint32_t n);

/**
 * Print Hertz in ii.dH format
 * where i = integer
 *       d = one decimal place
 *       H = the letter H for Hertz
 */
void HertzPrint(uint16_t myint);
#endif /* FIREFREQ_H */

