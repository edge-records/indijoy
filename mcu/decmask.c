#include "decmask.h"

void setdecmask(preset *thispreset, decmask *thismask) {
  // reset thismask
  memset(thismask, 0, sizeof(*thismask));

  // set cfg
  if ((*thispreset).buttonA == 4) (*thismask).cfg |= (1 << 4);
  if ((*thispreset).buttonB == 4) (*thismask).cfg |= (1 << 5);
  if ((*thispreset).buttonC == 4) (*thismask).cfg |= (1 << 6);

  // set f1
  if ((*thispreset).up == 1) {
    (*thismask).f1 |= (1 << 3);
  } else {
    if ((*thispreset).buttonA == 1) (*thismask).f1 |= (1 << 4);
    if ((*thispreset).buttonB == 1) (*thismask).f1 |= (1 << 5);
    if ((*thispreset).buttonC == 1) (*thismask).f1 |= (1 << 6);
  }

  // set f2
  if ((*thispreset).up == 2) {
    (*thismask).f2 |= (1 << 3);
  } else {
    if ((*thispreset).buttonA == 2) (*thismask).f2 |= (1 << 4);
    if ((*thispreset).buttonB == 2) (*thismask).f2 |= (1 << 5);
    if ((*thispreset).buttonC == 2) (*thismask).f2 |= (1 << 6);
  }

  // set f3
  if ((*thispreset).up == 3) {
    (*thismask).f3 |= (1 << 3);
  } else {
    if ((*thispreset).buttonA == 3) (*thismask).f3 |= (1 << 4);
    if ((*thispreset).buttonB == 3) (*thismask).f3 |= (1 << 5);
    if ((*thispreset).buttonC == 3) (*thismask).f3 |= (1 << 6);
  }

  // set up
  if ((*thispreset).up == 0) (*thismask).up |= (1 << 3);
  if ((*thispreset).up == 1) (*thismask).up |= (1 << 4);
  if ((*thispreset).up == 2) (*thismask).up |= (1 << 5);
  if ((*thispreset).up == 3) (*thismask).up |= (1 << 6);
}

