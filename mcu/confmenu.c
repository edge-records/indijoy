/**
## LED Menu

| Row | DispBeg | DispEnd | Meaning                               |
| --- | ------- | ------- | ------------------------------------- |
|   0 | PrE.1 * | PrE.9   | Preset select                         |
|   1 | C64  *  | A500    | Fire2/3 Mode (+PACo, PAAt)            |
|   2 | A. F1   | A.CFG   | Button A meaning (default F1)         |
|   3 | b. F1   | b.CFG   | Button B meaning (default F1)         |
|   4 | C. F1   | C.CFG   | Button C meaning (default F2)         |
|   5 | UP.UP * | UP. C   | Swap Up and a fire button             |
|   6 | FH. 6   | FL.35   | High RapidFire Frequency (default 25) |
|   7 | FL. 3   | FL.20   | Low RapidFire Frequency (default 5)   |
|   8 | o1   *  | o123    | RapidFire Effect on Fire Outputs      |
|   9 | dSP.0   | dSP.1 * | Display on or off (MAIN loop only)    |
|  10 | bri.0   | bri.7   | Brightness, 0-7, default 1            |
|  11 | Grt.0   | Grt.1 * | Greeting on or off "IndiJoy"          |
|  12 | rSEt    | n--y    | Reset this preset to Default          |
*/

#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include "confmenu.h"
#include "globaldefinitions.h"
#include "tm1637.h"
#include "millis.h"
#include "debounce.h"

EEMEM uint8_t EEpresetindex;

EEMEM preset EEpresets[9];

EEMEM extern preset prsdefault = {
  .mode = 0, // 0=C64, 1=A500, 2=Paddle Commodore, 3=Paddle Atari
  .buttonA = 1, // 1=Fire1, 2=Fire2, 3=Fire3, 4=CFG
  .buttonB = 1, // 1=Fire1, 2=Fire2, 3=Fire3, 4=CFG
  .buttonC = 2, // 1=Fire1, 2=Fire2, 3=Fire3, 4=CFG
  .up = 0, // 0=up(no change), 1-3= Button A-C
  .hifreq = 25, // 6-35
  .lofreq = 5, // 3-20
  .rfreach = 1, // bit 1 = F1, bit 2 = F2, bit 3 = F3
  .disp = 1, // 1=on, 0=off
  .brightness = 2, // 0-7
  .greeting = 1, // 1=on, 0=off
};

PROGMEM const uint8_t PRE[] = {
  0x73, // P
  0x50, // r
  0xF9, // E.
};

PROGMEM const uint8_t C64[] = {
  0x39, // C
  0x7D, // 6
  0x66, // 4
  0x00, // empty
};

PROGMEM const uint8_t A500[] = {
  0x77, // A
  0x6D, // 5
  0x3F, // 0
  0x3F, // 0
};

PROGMEM const uint8_t CFG[] = {
  0x39, // C
  0x71, // F
  0x3D, // G
};

PROGMEM const uint8_t DSP[] = {
  0x5E, // d
  0x6D, // S
  0xF3, // P.
};

PROGMEM const uint8_t BRI[] = {
  0x7C, // b
  0x50, // r
  0x90, // i.
};

PROGMEM const uint8_t GRT[] = {
  0x3D, // G
  0x50, // r
  0xF8, // t.
};

PROGMEM const uint8_t RSET[] = {
  0x50, // r
  0x6D, // S
  0x79, // E
  0x78, // t
};

PROGMEM const uint8_t NY[] = {
  0x54, // n
  0x40, // -
  0x40, // -
  0x6E, // y
};


unsigned long lastmil = 0, thismil = 0;
uint8_t reset = 0, toggle = 0;
uint8_t nocfgA = 0, nocfgB = 0, nocfgC = 0;


preset readpreset(uint8_t presetindex) {
  preset thispreset;
  eeprom_read_block(&thispreset, &EEpresets[presetindex], sizeof(thispreset));
  return thispreset;
}

void rereadpreset(uint8_t *presetindex, preset *thispreset) {
  *thispreset = readpreset(*presetindex);
  TM1637_init(1/*enable*/, ((*thispreset).brightness/*brightness*/));
}

void updatepreset(uint8_t presetindex, preset thispreset) {
  eeprom_update_block(&thispreset, &EEpresets[presetindex], sizeof(thispreset));
}

void initpreset(uint8_t presetindex) {
  preset defaultpreset;
  eeprom_read_block(&defaultpreset, &prsdefault, sizeof(defaultpreset));
  updatepreset(presetindex, defaultpreset);
}

uint8_t readpresetindex(void) {
  return eeprom_read_byte(&EEpresetindex);
}

void updatepresetindex(uint8_t presetindex) {
  eeprom_update_byte(&EEpresetindex, presetindex);
}

// 4 separate segments from Progmem
static void displaytype1(const uint8_t *elements) {
  uint8_t n;
  for (n=0; n < 4; n++) {
    TM1637_display_segments(n, pgm_read_byte(elements++));
  }
}

// 3 segments from Progmem, plus one digit
static void displaytype2(const uint8_t *elements, uint8_t column) {
  uint8_t n;
  for (n=0; n < 3; n++) {
    TM1637_display_segments(n, pgm_read_byte(elements++));
  }
  TM1637_display_segments(3, pgm_read_byte(&_digit2segments[column]));
}

// Helper function for button meaning entries
static void buttonhelper(uint8_t column) {
  if ( column <= 3 ) {
    TM1637_display_segments(1, 0); // empty
    TM1637_display_segments(2, 0x71); // F
    TM1637_display_segments(3, pgm_read_byte(&_digit2segments[column]));
    return;
  }
  uint8_t *elements = &CFG;
  for ( uint8_t n=1; n < 4; n++ ) {
    TM1637_display_segments(n, pgm_read_byte(elements++));
  }
}

static void displayrowcolumn(uint8_t row, uint8_t column, preset *thispreset, uint8_t *presetindex) {
  switch(row) {
    case 0 :
      displaytype2(PRE, column + 1);
      break;
    case 1 :
      if ( column == 0 ) {
        displaytype1(C64);
      }
      if ( column == 1 ) {
        displaytype1(A500);
      }
      break;
    case 2 :
      TM1637_display_segments(0, 0xF7); // A.
      buttonhelper(column);
      break;
    case 3 :
      TM1637_display_segments(0, 0xFC); // b.
      buttonhelper(column);
      break;
    case 4 :
      TM1637_display_segments(0, 0xB9); // C.
      buttonhelper(column);
      break;
    case 5 :
      TM1637_display_segments(0, 0x3E); // U
      TM1637_display_segments(1, 0xF3); // P.
      if ( column == 0 ) {
        TM1637_display_segments(2, 0x3E); // U
        TM1637_display_segments(3, 0x73); // P
      } else if ( column >= 1 ) {
        TM1637_display_segments(2, 0); // empty/
      }
      if ( column == 1) TM1637_display_segments(3, 0x77); //A
      if ( column == 2) TM1637_display_segments(3, 0x7C); //b
      if ( column == 3) TM1637_display_segments(3, 0x39); //C
      break;
    case 6 :
      TM1637_display_segments(0, 0x71); // F
      TM1637_display_segments(1, 0xF6); // H
      TM1637_PadPrint2(column);
      break;
    case 7 :
      TM1637_display_segments(0, 0x71); // F
      TM1637_display_segments(1, 0xB8); // L
      TM1637_PadPrint2(column);
      break;
    case 8 :
      thismil = millis();
      if ((thismil - lastmil) > 200) {
        toggle ^= 1;
        lastmil = thismil;
      }
      if ((toggle & 1) == 1) {
        TM1637_display_segments(0, 0x63); // o
      } else {
        TM1637_display_segments(0, 0); // empty
      }
      int8_t rfdisp[3] = {0,0,0};
      if ((column & 0b001) == 0b001) rfdisp[0] = 1;
      if ((column & 0b010) == 0b010) rfdisp[1] = 2;
      if ((column & 0b100) == 0b100) rfdisp[2] = 3;
      for (int8_t n=0; n < 3; n++) {
        if (rfdisp[n] > 0) {
          TM1637_display_segments(n+1, pgm_read_byte(&_digit2segments[rfdisp[n]]));
        } else {
          TM1637_display_segments(n+1, 0);
        }
      }
      break;
    case 9 :
      displaytype2(DSP, column);
      break;
    case 10 :
      displaytype2(BRI, column);
      break;
    case 11 :
      displaytype2(GRT, column);
      break;
    case 12 :
      switch(reset) {
        case 0 :
          displaytype1(RSET);
          break;
        case 1 :
          displaytype1(NY);
          break;
        case 2 :
          initpreset(*presetindex);
          eeprom_busy_wait();
          rereadpreset(presetindex, thispreset);
          TM1637_enable(0);
          _delay_ms(200);
          TM1637_enable(1);
          _delay_ms(200);
          TM1637_enable(0);
          _delay_ms(200);
          TM1637_enable(1);
          _delay_ms(200);
          TM1637_enable(0);
          _delay_ms(200);
          TM1637_enable(1);
          _delay_ms(200);
          reset = 0;
          break;
      }
      break;
  }
}

static int getcolumn(uint8_t row, preset *thispreset, uint8_t *presetindex) {
  uint8_t column;

  if (row == 0) {
    return *presetindex;
  } else {
    return *((uint8_t*)thispreset + (row - 1));
    // this means: Add row -1 multiplied by 1 (1 byte as in sizeof(uint8_t*) ) to the address of thispreset, and return its value.
    // Without (uint8_t*) it would multiply by the size of the struct, which is way too big obviously.
    // So instead of translating every row independently, we make use of the fact that the struct is in the same order as
    // the rows, and that all its values are uint8_t values.
  }
}

// direction 1=right, 0=left
// Shifts the 3 bits for brightness between 0 (0b000) and 7 (0b111)
static int shiftbrightness(int8_t direction, preset *thispreset) {
  int8_t column;

  column = (*thispreset).brightness;
  if (direction == 1) {
    if (column < 7) column++;
  } else {
    if (column > 0) column--;
  }
  (*thispreset).brightness = column;

  return(column);
}


void runmenu(uint8_t *presetindex, preset *thispreset) {
  uint8_t currentrow = 0;
  uint8_t currentjoy = 0;
  uint8_t cfgmask;
  while (~JOY & ((1 << BUTTONB) | (1 << BUTTONA) | (1 << BUTTONC))) {
    _delay_ms(10);
  }

  for(;;){
    wdt_reset();

    displayrowcolumn(currentrow, getcolumn(currentrow, thispreset, presetindex), thispreset, presetindex);

    debounce(~JOY, &currentjoy);

    if (currentjoy & (1 << UP_IN)) {
      currentjoy &= ~(1 << UP_IN);
      if (currentrow > 0) currentrow--;
    }
    if (currentjoy & (1 << DOWN_IN)) {
      currentjoy &= ~(1 << DOWN_IN);
      // Initialize a never used preset
      if ((currentrow == 0) && ((*thispreset).buttonA == 0)) {
        initpreset(*presetindex);
        eeprom_busy_wait();
        rereadpreset(presetindex, thispreset);
      }
      if (currentrow < 12) currentrow++;
    }
    if (currentjoy & (1 << RIGHT_IN)) {
      currentjoy &= ~(1 << RIGHT_IN);
      switch(currentrow) {
        case 0 :
          if (*presetindex < 8) (*presetindex)++;
          rereadpreset(presetindex, thispreset);
          break;
        case 1 :
          (*thispreset).mode = 1;
          break;
        case 2:
          nocfgA = 0;
          if ((*thispreset).up == 1) nocfgA = 1;
          if ((*thispreset).buttonA < (4 - nocfgA)) (*thispreset).buttonA++;
          break;
        case 3:
          nocfgB = 0;
          if ((*thispreset).up == 2) nocfgB = 1;
          if ((*thispreset).buttonB < (4 - nocfgB)) (*thispreset).buttonB++;
          break;
        case 4:
          nocfgC = 0;
          if ((*thispreset).up == 3) nocfgC = 1;
          if ((*thispreset).buttonC < (4 - nocfgC)) (*thispreset).buttonC++;
          break;
        case 5 :
          cfgmask = 0;
          if ((*thispreset).buttonA == 4) cfgmask = 1;
          if ((*thispreset).buttonB == 4) cfgmask |= 2;
          if ((*thispreset).buttonC == 4) cfgmask |= 4;

          for (uint8_t u = (*thispreset).up, p = 1; u<3; u++, p++) {
            if (! (cfgmask & (1 << u))) {
              (*thispreset).up += p;
              break;
            }
          }
          break;
        case 6 :
          if (((*thispreset).hifreq < 35) && ((*thispreset).hifreq - (*thispreset).lofreq >= 3)) (*thispreset).hifreq++;
          break;
        case 7 :
          if (((*thispreset).lofreq < 20) && ((*thispreset).hifreq - (*thispreset).lofreq > 3)) (*thispreset).lofreq++;
          break;
        case 8 :
          if ((*thispreset).rfreach < 7) (*thispreset).rfreach++;
          break;
        case 9 :
          (*thispreset).disp = 1;
          break;
        case 10 :
          TM1637_init(1/*enable*/, shiftbrightness(1, thispreset)/*brightness*/);
          break;
        case 11 :
          (*thispreset).greeting = 1;
          break;
        case 12 :
          if (reset < 2) reset++;
          break;
      }
    }
    if (currentjoy & (1 << LEFT_IN)) {
      currentjoy &= ~(1 << LEFT_IN);
      switch(currentrow) {
        case 0 :
          if (*presetindex > 0) (*presetindex)--;
          rereadpreset(presetindex, thispreset);
          break;
        case 1 :
          (*thispreset).mode = 0;
          break;
        case 2:
          if ((*thispreset).buttonA > 1) (*thispreset).buttonA--;
          break;
        case 3:
          if ((*thispreset).buttonB > 1) (*thispreset).buttonB--;
          break;
        case 4:
          if ((*thispreset).buttonC > 1) (*thispreset).buttonC--;
          break;
        case 5 :
          if (((*thispreset).up == 3) && ((*thispreset).buttonB == 4) && ((*thispreset).buttonA == 4)) {
            (*thispreset).up -= 3;
            break;
          }
          if (((*thispreset).up == 3) && ((*thispreset).buttonB == 4)) {
            (*thispreset).up -= 2;
            break;
          }
          if (((*thispreset).up == 2) && ((*thispreset).buttonA == 4)) {
            (*thispreset).up -= 2;
            break;
          }
          if ((*thispreset).up > 0) (*thispreset).up--;
          break;
        case 6 :
          if (((*thispreset).hifreq > 6) && ((*thispreset).hifreq - (*thispreset).lofreq > 3)) (*thispreset).hifreq--;
          break;
        case 7 :
          if (((*thispreset).lofreq > 3) && ((*thispreset).hifreq - (*thispreset).lofreq >= 3)) (*thispreset).lofreq--;
          break;
        case 8 :
          if ((*thispreset).rfreach > 1) (*thispreset).rfreach--;
          break;
        case 9 :
          (*thispreset).disp = 0;
          break;
        case 10 :
          TM1637_init(1/*enable*/, shiftbrightness(0, thispreset)/*brightness*/);
          break;
        case 11 :
          (*thispreset).greeting = 0;
          break;
        case 12 :
          if (reset > 0) reset--;
          break;
      }
    }

    // Save only
    if (currentjoy & (1 << BUTTONC)) {
      currentjoy &= ~(1 << BUTTONC);
      updatepreset(*presetindex, *thispreset);
      updatepresetindex(*presetindex);
      TM1637_enable(0);
      _delay_ms(200);
      TM1637_enable(1);
      _delay_ms(200);
      TM1637_enable(0);
      _delay_ms(200);
      TM1637_enable(1);
    }

    // Exit Configuration Menu
    if (currentjoy & (1 << BUTTONB)) {
      currentjoy &= ~(1 << BUTTONB);
      if ((*thispreset).buttonA == 0) {
        initpreset(*presetindex);
        rereadpreset(presetindex, thispreset);
      } else {
        updatepreset(*presetindex, *thispreset);
      }
      updatepresetindex(*presetindex);
      return;
    }

    // To debounce keys and joystick
    _delay_ms(10);
  }

}



