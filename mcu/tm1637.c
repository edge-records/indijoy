/**
 * Copyright (c) 2017-2018, Łukasz Marcin Podkalicki <lpodkalicki@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 **********************************************************************************
 *
 * This is ATtiny13/25/45/85 library for 4-Digit LED Display based on TM1637 chip.
 *
 * Features:
 * - display raw segments
 * - display digits
 * - display on/off
 * - brightness control
 *
 * References:
 * - library: https://github.com/lpodkalicki/attiny-tm1637-library
 * - documentation: https://github.com/lpodkalicki/attiny-tm1637-library/README.md
 * - TM1637 datasheet: https://github.com/lpodkalicki/attiny-tm1637-library/blob/master/docs/TM1637_V2.4_EN.pdf
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "tm1637.h"

#define	TM1637_DIO_HIGH()		(PORTD |= _BV(TM1637_DIO_PIN))
#define	TM1637_DIO_LOW()		(PORTD &= ~_BV(TM1637_DIO_PIN))
#define	TM1637_DIO_OUTPUT()		(DDRD |= _BV(TM1637_DIO_PIN))
#define	TM1637_DIO_INPUT()		(DDRD &= ~_BV(TM1637_DIO_PIN))
#define	TM1637_DIO_READ() 		(((PIND & _BV(TM1637_DIO_PIN)) > 0) ? 1 : 0)
#define	TM1637_CLK_HIGH()		(PORTD |= _BV(TM1637_CLK_PIN))
#define	TM1637_CLK_LOW()		(PORTD &= ~_BV(TM1637_CLK_PIN))

static void TM1637_send_config(const uint8_t enable, const uint8_t brightness);
static void TM1637_send_command(const uint8_t value);
static void TM1637_start(void);
static void TM1637_stop(void);
static uint8_t TM1637_write_byte(uint8_t value);

static uint8_t _config = TM1637_SET_DISPLAY_ON | TM1637_BRIGHTNESS_MAX;
static uint8_t segbuf_tobe[4] = {0,0,0,0};
static uint8_t segbuf_asis[4] = {0xff,0xff,0xff,0xff};
PROGMEM const uint8_t _digit2segments[] =
{
  0x3F, // 0
  0x06, // 1
  0x5B, // 2
  0x4F, // 3
  0x66, // 4
  0x6D, // 5
  0x7D, // 6
  0x07, // 7
  0x7F, // 8
  0x6F  // 9
};

uint16_t lock = 0;
void TM1637_lock(uint8_t lockkey) {
  for (;;){
    if ((lock & (1 << lockkey)) == 0) break;
  }
  lock = lock | (1 << lockkey);
}

void TM1637_unlock(uint8_t lockkey) {
  lock = lock & ~(1 << lockkey);
}

  void
TM1637_init(const uint8_t enable, const uint8_t brightness)
{
  TM1637_lock(0);
  DDRD |= (_BV(TM1637_DIO_PIN)|_BV(TM1637_CLK_PIN));
  PORTD &= ~(_BV(TM1637_DIO_PIN)|_BV(TM1637_CLK_PIN));
  TM1637_send_config(enable, brightness);
  TM1637_unlock(0);
}

  void
TM1637_enable(const uint8_t value)
{

  TM1637_lock(1);
  TM1637_send_config(value, _config & TM1637_BRIGHTNESS_MAX);
  TM1637_unlock(1);
}

  void
TM1637_set_brightness(const uint8_t value)
{
  TM1637_lock(2);
  TM1637_send_config(_config & TM1637_SET_DISPLAY_ON,
      value & TM1637_BRIGHTNESS_MAX);
  TM1637_unlock(2);
}

  void
TM1637_display_segments(const uint8_t position, const uint8_t segments)
{

  TM1637_lock(3);
  if (segbuf_asis[position] != segments) {
    TM1637_send_command(TM1637_CMD_SET_DATA | TM1637_SET_DATA_F_ADDR);
    TM1637_start();
    TM1637_write_byte(TM1637_CMD_SET_ADDR | (position & (TM1637_POSITION_MAX - 1)));
    TM1637_write_byte(segments);
    TM1637_stop();
    segbuf_asis[position] = segments;
  }
  TM1637_unlock(3);
}

  void
TM1637_display_digit(const uint8_t position, const uint8_t digit)
{
  TM1637_lock(4);
  uint8_t segments = (digit < 10 ? pgm_read_byte_near((uint8_t *)&_digit2segments + digit) : 0x00);

  segbuf_tobe[position] = segments | (segbuf_tobe[position] & 0x80);

  TM1637_display_segments(position, segbuf_tobe[position]);
  TM1637_unlock(4);
}

  void
TM1637_display_decdot(const uint8_t position)
{

  TM1637_lock(5);
  if (position <= 3) {
    segbuf_tobe[position] |= 0x80;
  }

  TM1637_display_segments(position, segbuf_tobe[position]);
  TM1637_unlock(5);
}

  void
TM1637_clear(void)
{
  TM1637_lock(6);
  uint8_t i;

  for (i = 0; i < TM1637_POSITION_MAX; ++i) {
    TM1637_display_segments(i, 0x00);
  }
  TM1637_unlock(6);
}

void TM1637_PadPrint4(uint16_t myint) {
  TM1637_lock(7);
  volatile uint8_t n,
          digit;

  for (n = 4; n > 0; n--) {
    digit = myint % 10;
    if (digit == 0 && myint == 0) {
      digit = 11;
    }
    myint /= 10;
    TM1637_display_digit(n - 1, digit);
  }
  TM1637_unlock(7);
}

void TM1637_PadPrint2(uint8_t myint) {
  TM1637_lock(8);
  volatile uint8_t n,
          digit;

  for (n = 4; n > 2; n--) {
    digit = myint % 10;
    if (digit == 0 && myint == 0) {
      digit = 11;
    }
    myint /= 10;
    TM1637_display_digit(n - 1, digit);
  }
  TM1637_unlock(8);
}

  void
TM1637_send_config(const uint8_t enable, const uint8_t brightness)
{

  _config = (enable ? TM1637_SET_DISPLAY_ON : TM1637_SET_DISPLAY_OFF) |
    (brightness > TM1637_BRIGHTNESS_MAX ? TM1637_BRIGHTNESS_MAX : brightness);

  TM1637_send_command(TM1637_CMD_SET_DSIPLAY | _config);
}

  void
TM1637_send_command(const uint8_t value)
{

  TM1637_start();
  TM1637_write_byte(value);
  TM1637_stop();
}

  void
TM1637_start(void)
{

  TM1637_DIO_HIGH();
  TM1637_CLK_HIGH();
  _delay_us(TM1637_DELAY_US);
  TM1637_DIO_LOW();
  _delay_us(TM1637_DELAY_US);
}

  void
TM1637_stop(void)
{

  TM1637_CLK_LOW();
  _delay_us(TM1637_DELAY_US);

  TM1637_DIO_LOW();
  _delay_us(TM1637_DELAY_US);

  TM1637_CLK_HIGH();
  _delay_us(TM1637_DELAY_US);

  TM1637_DIO_HIGH();
}

  uint8_t
TM1637_write_byte(uint8_t value)
{
  uint8_t i, ack;

  for (i = 0; i < 8; ++i, value >>= 1) {
    TM1637_CLK_LOW();
    _delay_us(TM1637_DELAY_US/2);

    if (value & 0x01) {
      TM1637_DIO_HIGH();
    } else {
      TM1637_DIO_LOW();
    }

    _delay_us(TM1637_DELAY_US/2);
    TM1637_CLK_HIGH();
    _delay_us(TM1637_DELAY_US);
  }

  TM1637_CLK_LOW();
  TM1637_DIO_INPUT();
  TM1637_DIO_HIGH();
  _delay_us(TM1637_DELAY_US);

  ack = TM1637_DIO_READ();
  if (ack) {
    TM1637_DIO_OUTPUT();
    TM1637_DIO_LOW();
  }
  _delay_us(TM1637_DELAY_US);

  TM1637_CLK_HIGH();
  _delay_us(TM1637_DELAY_US);

  TM1637_CLK_LOW();
  _delay_us(TM1637_DELAY_US);

  TM1637_DIO_OUTPUT();

  return ack;
}
